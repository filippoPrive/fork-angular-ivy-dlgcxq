import {Component, Input, OnInit} from '@angular/core';
import {Post} from "../../interfaces/post.interface";

@Component({
  selector: 'app-item-card',
  templateUrl: './item-card.component.html',
  styleUrls: ['./item-card.component.scss']
})
export class ItemCardComponent implements OnInit {

  @Input() obj: Post;
  @Input() lastChild;

  constructor() { }

  ngOnInit(): void {

  }

}
