import { Component } from '@angular/core';
import { pic } from '../assets/exercise';
import { posts} from "./mock-data/posts.mock";
import {Post} from "./interfaces/post.interface";

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  pic = pic;
  public posts: Post[] = posts;
}
